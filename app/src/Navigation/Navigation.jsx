import React from 'react';
import './Navigation.css';

class Navigation extends React.Component {
    render () {
        return (
            <div className={"container"}>
                <div className={"blog-header py-3"}>
                    <div className={"row flex-nowrap justify-content-between align-items-center"}>
                        <div className={"col-4 pt-1"}>
                            <a className={"text-muted"} href={"#"}> Something</a>
                        </div>

                        <div className={"col-4 text-center"}>
                            <a className={"blog-header-logo text-dark"} href={"#"}></a>
                        </div>

                        <div className={"col-4-flex justify-content-emd align-items-center"}>
                            <a className={"text-muted"} href={"#"}>
                            </a>
                            <a className={"btn btn-sm btn-outline-secondary"} href={"#"}>
                                Sign Up
                            </a>
                        </div>
                    </div>
                </div>
                <div className={"nav-scroller py-1 mb-2"}>
                    <nav className={"nav d-flex justify-content-between"}>
                        <a className={"p-2 text-muted"} href={"#"}>Programming</a>
                        <a className={"p-2 text-muted"} href={"#"}>Design</a>
                        <a className={"p-2 text-muted"} href={"#"}>Fitness</a>
                        <a className={"p-2 text-muted"} href={"#"}>Poles</a>
                        <a className={"p-2 text-muted"} href={"#"}>Misc</a>
                    </nav>
                </div>
            </div>
        )
    }
}

export default Navigation;
