import React, {Component} from 'react';

class Category extends Component {
    render() {
        return (
            <div className={"container"}>
                <div className={"row mb-2 "}>
                    <div className={"col-md-6"}>
                        <div className={"row no-gutters border rounded overflow-hidden flex-md mb-4 shadow-sm h-md-250 position-relative"}>
                            <div className={"col p-4 d-flex flex-column position-static"}>
                                <strong class={"d-inline-block mb-2 text-primary"}> Category One</strong>
                                <h3 className={"mb-0"}>Featured Posts</h3>
                                <div className={"mb-1  text-muted"}> NOv 12 </div>
                                <p className={"card-text mb-auto"}>
                                    This is a wider card with supporting text below as a natural lead-in to additional content.
                                </p>
                                <a href={"#"} className={"streched-link"}>Continue Reading...</a>
                            </div>
                            <div className={"col-auto d-none d-lg-block"}>
                            </div>
                        </div>
                    </div>
                    <div className={"col-md-6"}>
                        <div
                            className={"row no-gutters border rounded overflow-hidden flex-md mb-4 shadow-sm h-md-250 position-relative"}>
                            <div className={"col p-4 d-flex flex-column position-static"}>
                                <strong className={"d-inline-block mb-2 text-primary"}> Category One</strong>
                                <h3 className={"mb-0"}>Featured Posts</h3>
                                <div className={"mb-1  text-muted"}> NOv 12</div>
                                <p className={"card-text mb-auto"}>
                                    This is a wider card with supporting text below as a natural lead-in to additional
                                    content.
                                </p>
                                <a href={"#"} className={"streched-link"}>Continue Reading...</a>
                            </div>
                            <div className={"col-auto d-none d-lg-block"}>
                            </div>
                        </div>
                    </div>

                    <div className={"col-md-6"}>
                        <div
                            className={"row no-gutters border rounded overflow-hidden flex-md mb-4 shadow-sm h-md-250 position-relative"}>
                            <div className={"col p-4 d-flex flex-column position-static"}>
                                <strong className={"d-inline-block mb-2 text-primary"}> Category One</strong>
                                <h3 className={"mb-0"}>Featured Posts</h3>
                                <div className={"mb-1  text-muted"}> NOv 12</div>
                                <p className={"card-text mb-auto"}>
                                    This is a wider card with supporting text below as a natural lead-in to additional
                                    content.
                                </p>
                                <a href={"#"} className={"streched-link"}>Continue Reading...</a>
                            </div>
                            <div className={"col-auto d-none d-lg-block"}>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Category;
