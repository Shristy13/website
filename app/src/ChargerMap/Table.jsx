import React, {Component} from 'react';
import {Cell} from "./Cell";
import './Table.scss';

class Table extends Component {
    renderingHeadingRow = (_cell, cellIndex) => {
        const { headings } = this.props;

        return (
            <Cell
                key={`heading-${cellIndex}`}
                content={headings[cellIndex]}
                header={true}
            />
        )
    };

    renderRow = (_row, rowIndex) => {
        const {rows} = this.props;

        return (
            <tr key={ `row-${rowIndex}`}>
                {rows[rowIndex].map((_cell, cellIndex) => {
                    return (
                        <Cell
                            key={`${rowIndex}-${cellIndex}`}
                            content={rows[rowIndex][cellIndex]}
                        />
                    );
                })}
            </tr>
        );
    };

    render() {
        const {headings, rows} = this.props;

        this.renderingHeadingRow = this.renderingHeadingRow.bind(this);
        this.renderRow = this. renderRow.bind(this);

        const theadMarkup = (
            <tr key={"heading"}>
                {headings.map(this.renderingHeadingRow)}

            </tr>
        );

        const tbodyMarkup = rows.map(this.renderRow);
        return (
            <table className={"Table"}>
                <thead>{theadMarkup}</thead>
                <tbody>{tbodyMarkup}</tbody>
            </table>
        );
    }
}

export default Table;
