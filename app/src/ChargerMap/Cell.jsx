import React from 'react';

export const Cell = ({content, header}) => {
    const cellMarkup = header ? (
        <th className={" Cell Cell-header"}>
            {content}

        </th>
    ): (
        <td className={"Cell"}>
            {content}

        </td>
    );

    return (cellMarkup);
}
