import React, {Component} from 'react';
import Table from "./Table";

class TableContainer extends Component {
    render() {

        const chargers = [ {
            chargerName: "NL_GW_GVB_10_1",
            state: "Faulted",
            soc:0,
            busName: null,
            error: "Door is not closed",
            timestamp: "11/25/2019 9:47:49 AM",
        },
            {
                chargerName: "NL_GW_GVB_9_2",
                state: "Charging",
                soc:0,
                busName: null,
                error: "Door is not closed",
                timestamp: "11/25/2019 9:47:49 AM",
            },
            {
                chargerName: "NL_GW_GVB_9_3",
                state: "Available",
                soc:0,
                busName: null,
                error: "Door is not closed",
                timestamp: "11/25/2019 9:47:49 AM",
            }
        ]

        const headings = chargers.map((charger) => {
            const arr = charger.chargerName.split(/_/);
            return (arr.slice(Math.max(arr.length-2,1)));
        });

        const rows = [
            [
                'Pas 1',
                'Pas 1',
                28,
            ],
            [
                'Pas 1',
                'Pas 1',
                28,
            ],
            [
                'Pas 1',
                'Pas 1',
                28,
            ],
        ];

        return (
            <Table headings={headings} rows={rows} />
        );
    }
}

export default TableContainer;
